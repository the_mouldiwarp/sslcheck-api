package main

import (
	"crypto/tls"
	"fmt"
	"regexp"
	"strconv"
	"time"
)

// SSLdetail holds details of a sites ssl
type SSLdetail struct {
	Hostname   string   `json:"hostname"`
	Issuer     string   `json:"issuer"`
	NotBefore  string   `json:"notbefore"`
	NotAfter   string   `json:"notafter"`
	ExpiresIn  string   `json:"expiresin"`
	DomainList []string `json:"domainlist"`
}

// Sslconn establishes ssl connection and returns a tls.conn struct type
func Sslconn(s string) *tls.Conn {
	conf := &tls.Config{InsecureSkipVerify: true}

	conn, err := tls.Dial("tcp", s, conf)
	if err != nil {
		debug(fmt.Sprintf("Ooops something went wrong checking %s\n", s))
	}

	defer conn.Close()
	return conn
}

// FixMissingPort deals with presence or absence of a port in the request endpoint query string
func FixMissingPort(s string) string {
  type ep struct {
    domain string
    port   string
  }

  ret := ep{"", ""}

  rx := regexp.MustCompile(`(^[\w\.-]+)[:]?([\d]+)?$`)
  rxscan := rx.FindStringSubmatch(s)

  if len(rxscan) == 3 {
    ret.domain = rxscan[1]
    if rxscan[2] != "" {
      debug("A port has been specified in the domain string")
      ret.port = rxscan[2]
    } else {
      debug("No port has been specified in the domain string, defaulting to 443")
      ret.port = "443"
    }

    return fmt.Sprintf("%s:%s", ret.domain, ret.port)

  } else {
    debug("Domain regex matching has failed")
    err := fmt.Errorf("domain regex has failed on: %s", s)
	  if err != nil {
      debug("Domain regex error handling failed")
	  }
  }

  return s
}


// ExpiresIn takes an expiry time for a cert and compares it to current system time returning a string
// representaiton of time remaining on the certificate in days.
func ExpiresIn(de time.Time) string {
	dnow := time.Now()
	diffhours := de.Sub(dnow).Hours()
	diffdays := diffhours / float64(24)
	return strconv.FormatFloat(diffdays, 'f', 0, 64)
}

// GetIssuer returns the notAfter time object as a time object
func GetIssuer(tc *tls.Conn) string {
	return tc.ConnectionState().PeerCertificates[0].Issuer.CommonName
}

// NotAfter returns the notAfter time object as a time object
func NotAfter(tc *tls.Conn) time.Time {
	return tc.ConnectionState().PeerCertificates[0].NotAfter
}

// NotBefore returns the notAfter time object as a time object
func NotBefore(tc *tls.Conn) time.Time {
	return tc.ConnectionState().PeerCertificates[0].NotBefore
}

// DomainList lists all SAN entries for the site
func DomainList(tc *tls.Conn) []string {
	return tc.ConnectionState().PeerCertificates[0].DNSNames
}

// GetDetails combines the methods above to generate the reponse string for the API call
func GetDetails(s string) SSLdetail {
	d := FixMissingPort(s)
	tc := Sslconn(d)
	return SSLdetail{
		Hostname:   d,
		Issuer:     GetIssuer(tc),
		NotBefore:  NotBefore(tc).String(),
		NotAfter:   NotAfter(tc).String(),
		ExpiresIn:  ExpiresIn(NotAfter(tc)),
		DomainList: DomainList(tc)}
}
