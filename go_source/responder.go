package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func respond(w http.ResponseWriter, r *http.Request) {
	var output string
	domain := r.URL.Query().Get("domain")

	accept := r.Header.Get("accept")

	if domain != "" {
		data := GetDetails(domain)

		output = fmt.Sprintf("Hostname:   %s\n"+
			"Issuer:     %s \n"+
			"Not Before: %s \n"+
			"Not After:  %s \n"+
			"Expires in: %s Days \n"+
			"SAN List:\n",
			data.Hostname,
			data.Issuer,
			data.NotBefore,
			data.NotAfter,
			data.ExpiresIn)

		for _, dom := range data.DomainList {
			output += fmt.Sprintf("            %s\n", dom)
		}

		if accept == "application/json" {
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(data)
		} else {
			fmt.Fprintln(w, output)
		}

	} else {
		output = "Sorry did you provide a domain to test?, expected endpoint as a parameter.\n" +
			"Try ../search?endpoint=<domain>"
		fmt.Fprintln(w, output)
	}
}
