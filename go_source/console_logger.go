package main

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"

	c "github.com/TreyBastian/colourize"
)

func debug(info string) {
	if len(os.Getenv("DEBUG")) != 0 && len(os.Getenv("QUIETCONSOLE")) == 0 {
		_, file, line, _ := runtime.Caller(1)
		ds := fmt.Sprintf("[debug] %s:%d %v\n", filepath.Base(file), line, info)
		fmt.Println(c.Colourize(ds, c.Yellow, c.Blackbg))
	}
}

func info(info string) {
	if len(os.Getenv("QUIETCONSOLE")) == 0 {
		_, file, line, _ := runtime.Caller(1)
		ds := fmt.Sprintf("[info] %s:%d %v\n", filepath.Base(file), line, info)
		fmt.Println(c.Colourize(ds, c.Green, c.Blackbg))
	}
}

func forceinfo(info string) {
	_, file, line, _ := runtime.Caller(1)
	ds := fmt.Sprintf("[info] %s:%d %v\n", filepath.Base(file), line, info)
	fmt.Println(c.Colourize(ds, c.Green, c.Blackbg, c.Bold))
}
