package main

import (
	"flag"
	"fmt"
	"os"
)

// Main starts up the api interface
func main() {
	startTLS := flag.Bool("tls", false, "Use to start up a server on port 8443 with no TLS support, default is no tls on port 8080")
	commandline := flag.String("domain", "", "Dont run as a server, check local then exit")
	debug("Parsing command arguments")
	flag.Parse()

	if *commandline != "" {
		data := GetDetails(*commandline)

		output := fmt.Sprintf("Hostname:   %s\n"+
			"Issuer:     %s \n"+
			"Not Before: %s \n"+
			"Not After:  %s \n"+
			"Expires in: %s Days \n"+
			"SAN List:\n",
			data.Hostname,
			data.Issuer,
			data.NotBefore,
			data.NotAfter,
			data.ExpiresIn)

		for _, dom := range data.DomainList {
			output += fmt.Sprintf("            %s\n", dom)
		}

		fmt.Println(output)

		os.Exit(0)
	}

	handleRequests(*startTLS)
}
