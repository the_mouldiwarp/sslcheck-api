package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

// handleRequests defines the api interface
func handleRequests(e bool) {
	tlsenabled := os.Getenv("TLSENABLED")
	debug(fmt.Sprintf("TLSENABLED env variable set to: %s", tlsenabled))

	http.HandleFunc("/search", respond)

	if e || tlsenabled == "true" {
		key := os.Getenv("KEYPATH")
		cert := os.Getenv("PEMPATH")
		debug(fmt.Sprintf("Path to keyfile: %s", cert))
		debug(fmt.Sprintf("Path to certifile: %s", key))
		forceinfo("Starting server with TLS support on port 8443")
		log.Fatal(http.ListenAndServeTLS(":8443", cert, key, nil))
	} else {
		forceinfo("Starting server without TLS support on port 8080")
		log.Fatal(http.ListenAndServe(":8080", nil))
	}
}
