FROM golang:latest
WORKDIR /go/src/bitbucket.com/the_mouldiwarp/sslcheck-api
ADD ./go_source/* ./
RUN go get github.com/TreyBastian/colourize
RUN CGO_ENABLED=0 GOOS=linux go build -o sslcheck-api .


FROM ubuntu:latest
RUN useradd -u 10001 nonroot


FROM scratch
ENV PEMPATH=/etc/ssl/certs/custom/custom.pem
ENV KEYPATH=/etc/ssl/certs/custom/custom.key
COPY --from=0 /go/src/bitbucket.com/the_mouldiwarp/sslcheck-api/sslcheck-api /bin/sslcheck
COPY --from=1 /etc/passwd /etc/passwd
COPY /useless_ssl/* /etc/ssl/certs/custom/
EXPOSE 8443
EXPOSE 8080
USER nonroot
ENTRYPOINT ["/bin/sslcheck"]
